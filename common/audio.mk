PRODUCT_COPY_FILES += \
    vendor/gms/prebuilt/audio/alarms/bedside.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/bedside.ogg \
    vendor/gms/prebuilt/audio/alarms/frogs.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/frogs.ogg \
    vendor/gms/prebuilt/audio/alarms/ramble.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/ramble.ogg \
    vendor/gms/prebuilt/audio/alarms/coil.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/oil.ogg \
    vendor/gms/prebuilt/audio/alarms/kashio.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/kashio.ogg \
    vendor/gms/prebuilt/audio/alarms/transmission.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/transmission.ogg \
    vendor/gms/prebuilt/audio/alarms/nothing.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/nothing.ogg \
    vendor/gms/prebuilt/audio/alarms/incoming.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/incoming.ogg \
    vendor/gms/prebuilt/audio/alarms/munge.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/munge.ogg \
    vendor/gms/prebuilt/audio/alarms/prong.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/alarms/prong.ogg\
    vendor/gms/prebuilt/audio/notifications/oi!.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/oi!.ogg \
    vendor/gms/prebuilt/audio/notifications/bulb_one.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/bulb_one.ogg \
    vendor/gms/prebuilt/audio/notifications/bulb_two.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/bulb_two.ogg \
    vendor/gms/prebuilt/audio/notifications/guiro.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/guiro.ogg \
    vendor/gms/prebuilt/audio/notifications/volley.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/volley.ogg \
    vendor/gms/prebuilt/audio/notifications/squiggle.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/squiggle.ogg \
    vendor/gms/prebuilt/audio/notifications/isolator.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/isolator.ogg \
    vendor/gms/prebuilt/audio/notifications/gamma.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/gamma.ogg \
    vendor/gms/prebuilt/audio/notifications/beak.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/beak.ogg \
    vendor/gms/prebuilt/audio/notifications/nope.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/notifications/nope.ogg \
    vendor/gms/prebuilt/audio/ringtones/pneumatic.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/pneumatic.ogg\
    vendor/gms/prebuilt/audio/ringtones/abra.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/abra.ogg \
    vendor/gms/prebuilt/audio/ringtones/plot.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/plot.ogg \
    vendor/gms/prebuilt/audio/ringtones/beetle.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/beetle.ogg\
    vendor/gms/prebuilt/audio/ringtones/squirrels.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/squirrels.ogg \
    vendor/gms/prebuilt/audio/ringtones/snaps.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/snaps.ogg \
    vendor/gms/prebuilt/audio/ringtones/radiate.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/radiate.ogg \
    vendor/gms/prebuilt/audio/ringtones/tennis.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/tennis.ogg \
    vendor/gms/prebuilt/audio/ringtones/coded.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/coded.ogg \
    vendor/gms/prebuilt/audio/ringtones/scribble.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ringtones/scribble.ogg \
    vendor/gms/prebuilt/audio/ui/AttentionalHaptics.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/AttentionalHaptics.ogg \
    vendor/gms/prebuilt/audio/ui/audio_end.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/audio_end.ogg \
    vendor/gms/prebuilt/audio/ui/audio_initiate.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/audio_initiate.ogg \
    vendor/gms/prebuilt/audio/ui/camera_click.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/camera_click.ogg \
    vendor/gms/prebuilt/audio/ui/camera_focus.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/camera_focus.ogg \
    vendor/gms/prebuilt/audio/ui/ChargingStarted.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/ChargingStarted.ogg \
    vendor/gms/prebuilt/audio/ui/Dock.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/Dock.ogg \
    vendor/gms/prebuilt/audio/ui/Effect_Tick.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/Effect_Tick.ogg \
    vendor/gms/prebuilt/audio/ui/InCallNotification.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/InCallNotification.ogg \
    vendor/gms/prebuilt/audio/ui/KeypressDelete.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/KeypressDelete.ogg \
    vendor/gms/prebuilt/audio/ui/KeypressInvalid.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/KeypressInvalid.ogg \
    vendor/gms/prebuilt/audio/ui/KeypressReturn.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/KeypressReturn.ogg \
    vendor/gms/prebuilt/audio/ui/KeypressSpacebar.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/KeypressSpacebar.ogg \
    vendor/gms/prebuilt/audio/ui/KeypressStandard.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/KeypressStandard.ogg \
    vendor/gms/prebuilt/audio/ui/Lock.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/Lock.ogg \
    vendor/gms/prebuilt/audio/ui/LowBattery.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/LowBattery.ogg \
    vendor/gms/prebuilt/audio/ui/NFCFailure.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/NFCFailure.ogg \
    vendor/gms/prebuilt/audio/ui/NFCInitiated.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/NFCInitiated.ogg \
    vendor/gms/prebuilt/audio/ui/NFCSuccess.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/NFCSuccess.ogg \
    vendor/gms/prebuilt/audio/ui/NFCTransferComplete.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/NFCTransferComplete.ogg \
    vendor/gms/prebuilt/audio/ui/NFCTransferInitiated.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/NFCTransferInitiated.ogg \
    vendor/gms/prebuilt/audio/ui/screenshot.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/screenshot.ogg \
    vendor/gms/prebuilt/audio/ui/Trusted.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/Trusted.ogg \
    vendor/gms/prebuilt/audio/ui/Undock.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/Undock.ogg \
    vendor/gms/prebuilt/audio/ui/Unlock.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/Unlock.ogg \
    vendor/gms/prebuilt/audio/ui/VideoRecord.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/VideoRecord.ogg \
    vendor/gms/prebuilt/audio/ui/VideoStop.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/VideoStop.ogg \
    vendor/gms/prebuilt/audio/ui/WirelessChargingStarted.ogg:$(TARGET_COPY_OUT_PRODUCT)/media/audio/ui/WirelessChargingStarted.ogg

# Change default sounds
PRODUCT_PRODUCT_PROPERTIES += \
    ro.config.ringtone=radiate.ogg \
    ro.config.notification_sound=oi!.ogg \
    ro.config.alarm_alert=nothing.ogg
